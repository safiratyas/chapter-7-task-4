import '../App.css';

function Language() {
  return (
    <div className='App'>
        <header className='App=header'>
            <div className="language-item">
            <div className='language-name'>HTML & CSS</div>
            <img className='language-image' src='https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg' />
            </div>
        </header>
    </div>
  );
}

export default Language;
